*** Settings ***
Library    SeleniumLibrary
Resource    ${CURDIR}/SearchVariable.robot
Resource    ${CURDIR}/SearchKeyword.robot
Resource    ${CURDIR}/RegisterKeyword.robot

Suite Setup       Set Selenium Speed          0.5s
Test Setup        Open Website
Test Teardown     Close Browser
Suite Teardown    Close All Browsers

*** Test Cases ***
TC001-Input Data for login
    [Documentation]    ใช้สำหรับทดสอบในกรณีกรอกข้อมูลสำหรับ login
    Input data for login

TC002-Verify Search Filters and clear data search
    [Documentation]    ใช้สำหรับทดสอบในกรณีการกดเลือกข้อมูลในการค้นหาและการเคลียร์ข้อมูล
    [Setup]
    [Template]    Select Data for search
    0    0    0
    1    0    0
    0    1    0
    0    0    1
    1    1    0
    1    0    1
    0    1    1
    1    1    1
    [Teardown]

TC003 - Rows per page
    [Documentation]    ใช้สำหรับทดสอบในกรณีดูผลลัพท์ search เพิ่มเติม
    Input data for login
    Wait Until Page Contains    Search Filters
    Rows per page   

TC004 - Go to next page
    [Documentation]    ใช้สำหรับทดสอบในกรณีดูผลลัพท์ search เพิ่มเติมในหน้าถัดไป
    Input data for login
    Wait Until Page Contains    Search Filters  
    Go to next page

TC005 - Up button
    [Documentation]    ใช้สำหรับทดสอบในกรณีเมื่อเลื่อนลงดูผลลัพท์ search แล้วต้องการกลับไปดูข้อมูลด้านบนสุด
    Input data for login
    Wait Until Page Contains    Search Filters  
    Up button

TC006 - Input data pass
    [Documentation]    ใช้สำหรับทดสอบในกรณีการกรอกข้อมูลถูกต้องครบถ้วน
    [Setup]
    Open Website
    Input data for login
    Wait Until Page Contains    Search Filters
    Add user button
    Input Data for registor pass2
    Click Element    ${locator_btnsignup}
    Wait Until Page Contains         Register Success                    5s
    Click Element                    ${locator_btn_ok}
    Wait Until Page Contains         Search Filters
    [Teardown]
    
TC007 - Reset Data
    [Documentation]    ใช้สำหรับทดสอบในกรณีการเคลียร์ข้อมูล
    Input data for login
    Wait Until Page Contains    Search Filters
    Add user button
    Input Data for reset2
    Click Element               ${locator_reset}


TC008 - Input data register fail
    [Documentation]    ใช้สำหรับทดสอบในกรณีการกรอกข้อมูลไม่ถูกต้อง
    [Setup]
    [Template]    Input Data fail - Test Template2
    ${EMPTY}    สุจริต        nalinee@gmail.com    123456    0824698527    female    1    1    1    1    ${locator_err_firstname}    This field is required
    นลินี        ${EMPTY}     nalinee@gmail.com    123456    0824698527    female    1    1    1    1    ${locator_err_lastname}     This field is required
    นลินี         สุจริต        ${EMPTY}             123456    0824698527    female    1    1    1    1    ${locator_err_email}        This field is required
    นลินี         สุจริต        nalinee@gmail.com    ${EMPTY}  0824698527    female    1    1    1    1    ${locator_err_password}     This field is required
    นลินี         สุจริต        nalinee@gmail.com    123456      ${EMPTY}    female    1    1    1    1    ${locator_err_phone}        This field is required
    นลินี         สุจริต        nalinee@gmail.com    123456    0824698527       emp    1    1    1    1    ${locator_err_gender}       This field is required
    นลินี         สุจริต        nalinee@gmail.com    123456    0824698527    female    0    1    1    1    ${locator_err_checkbox}     This field is required
    นลินี         สุจริต        nalinee@gmail.com    123456    0824698527    female    1    0    1    1    ${locator_err_nationality}  This field is required
    นลินี         สุจริต        nalinee@gmail.com    123456    0824698527    female    1    1    0    1    ${locator_err_role}         This field is required
    นลินี         สุจริต        nalinee@gmail.com    123456    0824698527    female    1    1    1    0    ${locator_err_plan}         This field is required
    [Teardown]

TC009 - Back button
    [Documentation]    ใช้สำหรับกรณีทดสอบปุ่มย้อนกลับ
    Input data for login
    Add user button
    Back button

TC010 - Log out
    [Documentation]    ใช้สำหรับกรณีทดสอบปุ่ม logout
    Input data for login
    Wait Until Page Contains    Search Filters
    Icon user for Log out

TC011 - Noti button
    [Documentation]    ใช้สำหรับกรณีทดสอบปุ่ม Notification
    [Tags]    Done
    Input data for login
    Wait Until Page Contains    Search Filters
    Noti button

    



    
