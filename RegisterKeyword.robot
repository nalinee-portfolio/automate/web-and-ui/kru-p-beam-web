*** Settings ***
Library     SeleniumLibrary
Resource    ${CURDIR}/RegisterVariable.robot
Resource    ${CURDIR}/SearchKeyword.robot

*** Keywords ***
Open Webbrowser
    Open Browser                ${url}    ${Browser}
    Wait Until Page Contains    Kru P' Beam    5s
    Maximize Browser Window

Input Data for registor pass
    Input Text                  ${locator_firstname}    นลินี
    Input Text                  ${locator_lastname}     สุจริต
    Input Text                  ${locator_email}        Beam1234@gmail.com
    Input Password              ${locator_password}     1234567890
    Input Text                  ${locator_mobilephone}  0824814195      
    Select Radio Button         ${locator_gender}       female
    Select Checkbox             ${locator_checkboxSQL}
    Select Checkbox             ${locator_checkboxmanual} 
    Select Checkbox             ${locator_checkboxauto}   
    Select Checkbox             ${locator_checkboxauto2}    
    Select list Nationality
    Select list SQL
    Select list Plan
    Click Element                    ${locator_loginbutton}   
    Wait Until Page Contains         Register Success                    5s
    Click Element                    ${locator_btn_ok}
    Wait Until Page Contains         Welcome to Kru P' Beam!             5s
    Wait Until Element Is Visible    ${locator_email}                    5s

Input Data for registor pass2
    Input Text                  ${locator_firstname}    นลินี
    Input Text                  ${locator_lastname}     สุจริต
    Input Text                  ${locator_email}        Beam1234@gmail.com
    Input Password              ${locator_password}     1234567890
    Input Text                  ${locator_mobilephone}  0824814195      
    Select Radio Button         ${locator_gender}       female
    Select Checkbox             ${locator_checkboxSQL}
    Select Checkbox             ${locator_checkboxmanual} 
    Select Checkbox             ${locator_checkboxauto}   
    Select Checkbox             ${locator_checkboxauto2}    
    Select list Nationality
    Select list SQL
    Select list Plan

Select list SQL
    Click Element                    ${locator_select-role}
    Wait Until Element Is Visible    ${locator_select-role-list}    5s
    Click Element                    ${locator_select-role-list}    

Select list Nationality
    Click Element                    ${locator_nationality}
    Wait Until Element Is Visible    ${locator_select-nation}       5s     
    Click Element                    ${locator_select-nation}

Select list Plan
    Click Element                    ${locator_plan}
    Wait Until Element Is Visible    ${locator_select-plan}         5s   
    Click Element                    ${locator_choose-plan}

Input Data for reset
    Input Text                  ${locator_firstname}    นลินี
    Input Text                  ${locator_lastname}     สุจริต
    Input Text                  ${locator_email}        Beam1234@gmail.com
    Input Password              ${locator_password}     1234567890
    Input Text                  ${locator_mobilephone}  0824814195      
    Select Radio Button         ${locator_gender}       female
    Select Checkbox             ${locator_checkboxSQL}
    Select Checkbox             ${locator_checkboxmanual} 
    Select Checkbox             ${locator_checkboxauto}   
    Select Checkbox             ${locator_checkboxauto2}    
    Select list Nationality
    Select list SQL
    Select list Plan

Input Data for reset2
    Input Text                  ${locator_firstname}    นลินี
    Input Text                  ${locator_lastname}     สุจริต
    Input Text                  ${locator_email}        Beam1234@gmail.com
    Input Password              ${locator_password}     1234567890  

Input Data for fail - Wrong Email
    Input Text                  ${locator_firstname}    นลินี
    Input Text                  ${locator_lastname}     สุจริต
    Input Text                  ${locator_email}        Beam1234gmail.com
    Input Password              ${locator_password}     1234567890
    Input Text                  ${locator_mobilephone}  0824814195      
    Select Radio Button         ${locator_gender}       female
    Select Checkbox             ${locator_checkboxSQL}
    Select Checkbox             ${locator_checkboxmanual} 
    Select Checkbox             ${locator_checkboxauto}   
    Select Checkbox             ${locator_checkboxauto2}    
    Select list Nationality
    Select list SQL
    Select list Plan
    Click Element                    ${locator_loginbutton}

Verify Error message Email2
    ${txt2}=    Get Text           ${locator_err_wrongmail}
    Should Be Equal As Strings     ${txt2}                      Invalid email address

Verify Error msg
    [Arguments]                   ${locator}    ${msg}=${EMPTY}
    ${txt}=    Get Text           ${locator}   
    Should Be Equal As Strings    ${txt}        ${msg}

Input Data fail - Test Template
    [Arguments]        ${Firstname}    ${Lastname}    ${Email}    ${Password}    ${Phone}    ${Gender}    ${Checkbox}    ${National}    ${Role}    ${Plan}    ${locator}    ${msg}
    Open Webbrowser
    Input Text                  ${locator_firstname}    ${Firstname}
    Input Text                  ${locator_lastname}     ${Lastname}
    Input Text                  ${locator_email}        ${Email}
    Input Password              ${locator_password}     ${Password}
    Input Text                  ${locator_mobilephone}  ${Phone}    
    IF  "${Gender}" != "emp"
        Select Radio Button    ${locator_gender}    ${Gender}
    END
    
    IF      ${Checkbox} == 1
        Select Checkbox             ${locator_checkboxSQL}
    
    ELSE IF  ${Checkbox} == 2
        Select Checkbox             ${locator_checkboxSQL}
        Select Checkbox             ${locator_checkboxmanual}     
    ELSE IF  ${Checkbox} == 3
        Select Checkbox             ${locator_checkboxSQL}
        Select Checkbox             ${locator_checkboxmanual}
        Select Checkbox             ${locator_checkboxauto}          
    ELSE IF  ${Checkbox} == 4
        Select Checkbox             ${locator_checkboxSQL}
        Select Checkbox             ${locator_checkboxmanual}
        Select Checkbox             ${locator_checkboxauto} 
        Select Checkbox             ${locator_checkboxauto2}  
    END

    IF  ${National} == 1
        Select list Nationality
    END
    
    IF  ${Role} == 1
        Select list SQL
    END

    IF  ${Plan} == 1
        Select list Plan
    END

    Click Element                    ${locator_loginbutton}
    Verify Error msg    ${locator}    ${msg}  
    Close Browser

Input Data fail - Test Template2
    [Arguments]        ${Firstname}    ${Lastname}    ${Email}    ${Password}    ${Phone}    ${Gender}    ${Checkbox}    ${National}    ${Role}    ${Plan}    ${locator}    ${msg}
    Open Website
    Input data for login
    Add user button
    Input Text                  ${locator_firstname}    ${Firstname}
    Input Text                  ${locator_lastname}     ${Lastname}
    Input Text                  ${locator_email}        ${Email}
    Input Password              ${locator_password}     ${Password}
    Input Text                  ${locator_mobilephone}  ${Phone}    
    IF  "${Gender}" != "emp"
        Select Radio Button    ${locator_gender}    ${Gender}
    END
    
    IF      ${Checkbox} == 1
        Select Checkbox             ${locator_checkboxSQL}
    
    ELSE IF  ${Checkbox} == 2
        Select Checkbox             ${locator_checkboxSQL}
        Select Checkbox             ${locator_checkboxmanual}     
    ELSE IF  ${Checkbox} == 3
        Select Checkbox             ${locator_checkboxSQL}
        Select Checkbox             ${locator_checkboxmanual}
        Select Checkbox             ${locator_checkboxauto}          
    ELSE IF  ${Checkbox} == 4
        Select Checkbox             ${locator_checkboxSQL}
        Select Checkbox             ${locator_checkboxmanual}
        Select Checkbox             ${locator_checkboxauto} 
        Select Checkbox             ${locator_checkboxauto2}  
    END

    IF  ${National} == 1
        Select list Nationality
    END
    
    IF  ${Role} == 1
        Select list SQL
    END

    IF  ${Plan} == 1
        Select list Plan
    END

    Click Element                    ${locator_btnsignup}
    Verify Error msg    ${locator}    ${msg}  
    Close Browser
    