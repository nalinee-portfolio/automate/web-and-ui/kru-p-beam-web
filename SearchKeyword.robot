*** Settings ***
Library    SeleniumLibrary
Resource    ${CURDIR}/SearchVariable.robot

*** Keywords ***
Open Website
    Open Browser                ${url}    ${Browser}
    Wait Until Page Contains    Kru P' Beam    3s
    Maximize Browser Window

Input data for login
    Input Text        ${locator_email}    user.test@krupbeam.com
    Input Text        ${locator_password1}    123456789
    Click Element     ${locator_btnlogin}
    Wait Until Page Contains    Search Filters

Select Role
    Click Element                    ${locator_selectrole}
    Wait Until Element Is Visible    ${locator_selectrole-list}        2s   
    Click Element                    ${locator_selectrole-list}

Select Plan
    Click Element                    ${locator_selectplan}
    Wait Until Element Is Visible    ${locator_selectplan-list}        2s   
    Click Element                    ${locator_selectplan-list}

Select Status
    Click Element                    ${locator_selectstatus}
    Wait Until Element Is Visible    ${locator_selectstatus-list}        2s   
    Click Element                    ${locator_selectstatus-list}

Select Data for search
    Open Website
    Input data for login

    [Arguments]    ${Role}    ${Plan}    ${Status}
    IF  ${Role} == 1
    Select Role 
    END

    IF  ${Plan} == 1
    Select Plan
    END

    IF  ${Status} == 1
    Select Status    
    END
    Click Element    ${locator_btnsearch}
    Wait Until Element Is Visible    ${locator_User}
    Click Element    ${locator_btnclear}
    Close Browser

Add user button
    Wait Until Element Is Visible    ${locator_adduser}
    Click Element    ${locator_adduser}
    Wait Until Page Contains    Kru P' Beam

Back button
    Click Element    ${locator_btnback} 
    Wait Until Page Contains    Search Filters

Icon user for Log out
    Click Element    ${locator_User}
    Wait Until Page Contains    User Test
    Click Element    ${locator_btnlogout}
    Wait Until Page Contains    Welcome to Kru P' Beam!

Rows per page
    Click Element                    ${locator_btnsearch}
    Scroll Element Into View         ${locator_rows}
    Click Element                    ${locator_rows}
    Wait Until Element Is Visible    ${locator_select_rows}        2s   
    Click Element                    ${locator_select_rows}
    
Go to next page
    Click Element                    ${locator_btnsearch}
    Scroll Element Into View         ${locator_rows}
    Click Element                    ${locator_next_page}
    Wait Until Element Is Visible    ${locator_next_page2}

Up button
    Click Element                    ${locator_btnsearch}
    Scroll Element Into View         ${locator_end_page}
    Wait Until Element Is Visible    ${locator_up}
    Click Button                     ${locator_up}
    Wait Until Page Contains         Search Filters

Noti button
    Click Element    ${locator_noti}
    Wait Until Page Contains    Notifications
    