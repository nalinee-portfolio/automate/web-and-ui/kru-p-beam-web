*** Variables ***
${url}                       https://automate-test.stpb-digital.com/register/
${Browser}                   Chrome
${locator_firstname}         id=firstname
${locator_lastname}          id=lastname
${locator_email}             id=email
${locator_password}          id=password
${locator_mobilephone}       id=mobile-phone
${locator_gender}            validation-basic-radio
${locator_checkboxSQL}       name=courses.SQL
${locator_checkboxmanual}    name=courses.Test Manual
${locator_checkboxauto}      name=courses.Automate Test
${locator_checkboxauto2}     name=courses.Automate Test2 
${locator_select-role}       id=select-role
${locator_select-role-list}  xpath=//*[@id="menu-"]/div[3]/ul/li[1]
${locator_loginbutton}       id=btn-sign-up
${locator_reset}             id=reset
${locator_nationality}       id=nationality
${locator_select-nation}     xpath=//*[@id="menu-"]/div[3]/ul/li[221]
${locator_plan}              id=select-plan
${locator_select-plan}       xpath=//*[@id="menu-"]/div[3]/ul/li[1]
${locator_choose-plan}       xpath=//*[@id="menu-"]/div[3]/ul/li[3]
${locator_btn_ok}            id=btn-ok
${locator_err_firstname}     id=error-firstname
${locator_err_lastname}      id=error-lastname
${locator_err_email}          id=error-email
${locator_err_password}       id=error-password
${locator_err_phone}          id=error-mobile-phone
${locator_err_gender}         id=validation-basic-gender
${locator_err_checkbox}       id=validation-basic-courses
${locator_err_nationality}    id=validation-basic-nationality
${locator_err_role}           id=validation-role
${locator_err_plan}           id=validation-plan
${locator_err_wrongmail}      id=error-email