*** Settings ***
Library           SeleniumLibrary
Resource          ${CURDIR}/RegisterVariable.robot
Resource          ${CURDIR}/RegisterKeyword.robot
Suite Setup       Set Selenium Speed          0.5s
Test Setup        Open Webbrowser
Test Teardown     Close Browser
Suite Teardown    Close All Browsers


*** Test Cases ***

TC001-Input Data Pass
    [Documentation]    ใช้สำหรับทดสอบในกรณีกรอกข้อมูลถูกต้องครบถ้วน
    Input Data for registor pass

TC002-Reset Data
    [Documentation]    ใช้สำหรับทดสอบในกรณีเคลียร์ข้อมูล
    Input Data for reset
    Click Element    ${locator_reset}  

TC003-Verify case fail
    [Documentation]    ใช้สำหรับทดสอบในกรณีกรอกข้อมูลไม่ครบถ้วน
    [Tags]    pass
    [Setup]
    [Template]    Input Data fail - Test Template
    ${EMPTY}    สุจริต        nalinee@gmail.com    123456    0824698527    female    1    1    1    1    ${locator_err_firstname}    This field is required
    นลินี        ${EMPTY}     nalinee@gmail.com    123456    0824698527    female    1    1    1    1    ${locator_err_lastname}     This field is required
    นลินี         สุจริต        ${EMPTY}             123456    0824698527    female    1    1    1    1    ${locator_err_email}        This field is required
    นลินี         สุจริต        nalinee@gmail.com    ${EMPTY}  0824698527    female    1    1    1    1    ${locator_err_password}     This field is required
    นลินี         สุจริต        nalinee@gmail.com    123456      ${EMPTY}    female    1    1    1    1    ${locator_err_phone}        This field is required
    นลินี         สุจริต        nalinee@gmail.com    123456    0824698527       emp    1    1    1    1    ${locator_err_gender}       This field is required
    นลินี         สุจริต        nalinee@gmail.com    123456    0824698527    female    0    1    1    1    ${locator_err_checkbox}     This field is required
    นลินี         สุจริต        nalinee@gmail.com    123456    0824698527    female    1    0    1    1    ${locator_err_nationality}  This field is required
    นลินี         สุจริต        nalinee@gmail.com    123456    0824698527    female    1    1    0    1    ${locator_err_role}         This field is required
    นลินี         สุจริต        nalinee@gmail.com    123456    0824698527    female    1    1    1    0    ${locator_err_plan}         This field is required
    [Teardown]
