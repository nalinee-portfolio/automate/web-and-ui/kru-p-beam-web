*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${url}                       https://automate-test.stpb-digital.com/register/
${Browser}                   Chrome
${locator_firstname}         id=firstname
${locator_lastname}          id=lastname
${locator_email}             id=email
${locator_password}          id=password
${locator_mobilephone}       id=mobile-phone
${locator_gender}            validation-basic-radio
${locator_checkboxSQL}       name=courses.SQL
${locator_checkboxmanual}    name=courses.Test Manual
${locator_checkboxauto}      name=courses.Automate Test
${locator_checkboxauto2}     name=courses.Automate Test2 
${locator_select-role}       id=select-role
${locator_select-role-list}  xpath=//*[@id="menu-"]/div[3]/ul/li[1]
${locator_loginbutton}       id=btn-sign-up
${locator_reset}             id=reset
${locator_nationality}       id=nationality
${locator_select-nation}     xpath=//*[@id="menu-"]/div[3]/ul/li[221]
${locator_plan}              id=select-plan
${locator_select-plan}       xpath=//*[@id="menu-"]/div[3]/ul/li[1]
${locator_choose-plan}       xpath=//*[@id="menu-"]/div[3]/ul/li[3]
${locator_btn_ok}            id=btn-ok
${locator_err_firstname}     id=error-firstname
${locator_err_lastname}      id=error-lastname
${locator_err_email}          id=error-email
${locator_err_password}       id=error-password
${locator_err_phone}          id=error-mobile-phone
${locator_err_gender}         id=validation-basic-gender
${locator_err_checkbox}       id=validation-basic-courses
${locator_err_nationality}    id=validation-basic-nationality
${locator_err_role}           id=validation-role
${locator_err_plan}           id=validation-plan
${locator_err_wrongmail}      id=error-email

*** Keywords ***
Open Webbrowser
    Set Selenium Speed          0.5s
    Open Browser                ${url}    ${Browser}
    Wait Until Page Contains    Kru P' Beam    5s
    Maximize Browser Window

Input Data for registor pass
    Input Text                  ${locator_firstname}    นลินี
    Input Text                  ${locator_lastname}     สุจริต
    Input Text                  ${locator_email}        Beam1234@gmail.com
    Input Password              ${locator_password}     1234567890
    Input Text                  ${locator_mobilephone}  0824814195      
    Select Radio Button         ${locator_gender}       female
    Select Checkbox             ${locator_checkboxSQL}
    Select Checkbox             ${locator_checkboxmanual} 
    Select Checkbox             ${locator_checkboxauto}   
    Select Checkbox             ${locator_checkboxauto2}    
    Select list Nationality
    Select list SQL
    Select list Plan
    Click Element                    ${locator_loginbutton}   
    Wait Until Page Contains         Register Success                    5s
    Click Element                    ${locator_btn_ok}
    Wait Until Page Contains         Welcome to Kru P' Beam!             5s
    Wait Until Element Is Visible    ${locator_email}                    5s

Select list SQL
    Click Element                    ${locator_select-role}
    Wait Until Element Is Visible    ${locator_select-role-list}    5s
    Click Element                    ${locator_select-role-list}    

Select list Nationality
    Click Element                    ${locator_nationality}
    Wait Until Element Is Visible    ${locator_select-nation}       5s     
    Click Element                    ${locator_select-nation}

Select list Plan
    Click Element                    ${locator_plan}
    Wait Until Element Is Visible    ${locator_select-plan}         5s   
    Click Element                    ${locator_choose-plan}

Input Data 
    Input Text                  ${locator_firstname}    นลินี
    Input Text                  ${locator_lastname}     สุจริต
    Input Text                  ${locator_email}        Beam1234@gmail.com
    Input Password              ${locator_password}     1234567890
    Input Text                  ${locator_mobilephone}  0824814195      
    Select Radio Button         ${locator_gender}       female
    Select Checkbox             ${locator_checkboxSQL}
    Select Checkbox             ${locator_checkboxmanual} 
    Select Checkbox             ${locator_checkboxauto}   
    Select Checkbox             ${locator_checkboxauto2}    
    Select list Nationality
    Select list SQL
    Select list Plan

Input Data for fail - Firstname
    Input Text                  ${locator_firstname}    ${EMPTY}
    Input Text                  ${locator_lastname}     สุจริต
    Input Text                  ${locator_email}        Beam1234@gmail.com
    Input Password              ${locator_password}     1234567890
    Input Text                  ${locator_mobilephone}  0824814195      
    Select Radio Button         ${locator_gender}       female
    Select Checkbox             ${locator_checkboxSQL}
    Select Checkbox             ${locator_checkboxmanual} 
    Select Checkbox             ${locator_checkboxauto}   
    Select Checkbox             ${locator_checkboxauto2}    
    Select list Nationality
    Select list SQL
    Select list Plan
    Click Element                    ${locator_loginbutton}

Verify Error message
    ${txt}=    Get Text           ${locator_err_firstname}   
    Should Be Equal As Strings    ${txt}                       This field is required

Input Data for fail - Lastname
    Input Text                  ${locator_firstname}    นลินี
    Input Text                  ${locator_lastname}     ${EMPTY}
    Input Text                  ${locator_email}        Beam1234@gmail.com
    Input Password              ${locator_password}     1234567890
    Input Text                  ${locator_mobilephone}  0824814195      
    Select Radio Button         ${locator_gender}       female
    Select Checkbox             ${locator_checkboxSQL}
    Select Checkbox             ${locator_checkboxmanual} 
    Select Checkbox             ${locator_checkboxauto}   
    Select Checkbox             ${locator_checkboxauto2}    
    Select list Nationality
    Select list SQL
    Select list Plan
    Click Element                    ${locator_loginbutton}

Verify Error message Lastname
    ${txt1}=    Get Text           ${locator_err_lastname} 
    Should Be Equal As Strings     ${txt1}                      This field is required

Input Data for fail - Email
    Input Text                  ${locator_firstname}    นลินี
    Input Text                  ${locator_lastname}     สุจริต
    Input Text                  ${locator_email}        ${EMPTY}
    Input Password              ${locator_password}     1234567890
    Input Text                  ${locator_mobilephone}  0824814195      
    Select Radio Button         ${locator_gender}       female
    Select Checkbox             ${locator_checkboxSQL}
    Select Checkbox             ${locator_checkboxmanual} 
    Select Checkbox             ${locator_checkboxauto}   
    Select Checkbox             ${locator_checkboxauto2}    
    Select list Nationality
    Select list SQL
    Select list Plan
    Click Element                    ${locator_loginbutton}

Verify Error message Email
    ${txt2}=    Get Text           ${locator_err_email}
    Should Be Equal As Strings     ${txt2}                      This field is required

Input Data for fail - Password
    Input Text                  ${locator_firstname}    นลินี
    Input Text                  ${locator_lastname}     สุจริต
    Input Text                  ${locator_email}        Beam1234@gmail.com
    Input Password              ${locator_password}     ${EMPTY}
    Input Text                  ${locator_mobilephone}  0824814195      
    Select Radio Button         ${locator_gender}       female
    Select Checkbox             ${locator_checkboxSQL}
    Select Checkbox             ${locator_checkboxmanual} 
    Select Checkbox             ${locator_checkboxauto}   
    Select Checkbox             ${locator_checkboxauto2}    
    Select list Nationality
    Select list SQL
    Select list Plan
    Click Element                    ${locator_loginbutton}

Verify Error message Password
    ${txt3}=    Get Text           ${locator_err_password}
    Should Be Equal As Strings     ${txt3}                      This field is required

Input Data for fail - Phone
    Input Text                  ${locator_firstname}    นลินี
    Input Text                  ${locator_lastname}     สุจริต
    Input Text                  ${locator_email}        Beam1234@gmail.com
    Input Password              ${locator_password}     1234567890
    Input Text                  ${locator_mobilephone}  ${EMPTY}      
    Select Radio Button         ${locator_gender}       female
    Select Checkbox             ${locator_checkboxSQL}
    Select Checkbox             ${locator_checkboxmanual} 
    Select Checkbox             ${locator_checkboxauto}   
    Select Checkbox             ${locator_checkboxauto2}    
    Select list Nationality
    Select list SQL
    Select list Plan
    Click Element                    ${locator_loginbutton}

Verify Error message Phone
    ${txt4}=    Get Text           ${locator_err_phone}
    Should Be Equal As Strings     ${txt4}                      This field is required

Input Data for fail - Gender
    Input Text                  ${locator_firstname}    นลินี
    Input Text                  ${locator_lastname}     สุจริต
    Input Text                  ${locator_email}        Beam1234@gmail.com
    Input Password              ${locator_password}     1234567890
    Input Text                  ${locator_mobilephone}  0824814195    
    # Select Radio Button         ${locator_gender}       female
    Select Checkbox             ${locator_checkboxSQL}
    Select Checkbox             ${locator_checkboxmanual} 
    Select Checkbox             ${locator_checkboxauto}   
    Select Checkbox             ${locator_checkboxauto2}    
    Select list Nationality
    Select list SQL
    Select list Plan
    Click Element                    ${locator_loginbutton}

Verify Error message Gender
    ${txt5}=    Get Text           ${locator_err_gender}
    Should Be Equal As Strings     ${txt5}                      This field is required

Input Data for fail - Checkbox
    Input Text                  ${locator_firstname}    นลินี
    Input Text                  ${locator_lastname}     สุจริต
    Input Text                  ${locator_email}        Beam1234@gmail.com
    Input Password              ${locator_password}     1234567890
    Input Text                  ${locator_mobilephone}  0824814195    
    Select Radio Button         ${locator_gender}       female
    # Select Checkbox             ${locator_checkboxSQL}
    # Select Checkbox             ${locator_checkboxmanual} 
    # Select Checkbox             ${locator_checkboxauto}   
    # Select Checkbox             ${locator_checkboxauto2}    
    Select list Nationality
    Select list SQL
    Select list Plan
    Click Element                    ${locator_loginbutton}

Verify Error message Checkbox
    ${txt6}=    Get Text           ${locator_err_checkbox}
    Should Be Equal As Strings     ${txt6}                      This field is required

Input Data for fail - Nationnality
    Input Text                  ${locator_firstname}    นลินี
    Input Text                  ${locator_lastname}     สุจริต
    Input Text                  ${locator_email}        Beam1234@gmail.com
    Input Password              ${locator_password}     1234567890
    Input Text                  ${locator_mobilephone}  0824814195    
    Select Radio Button         ${locator_gender}       female
    Select Checkbox             ${locator_checkboxSQL}
    Select Checkbox             ${locator_checkboxmanual} 
    Select Checkbox             ${locator_checkboxauto}   
    Select Checkbox             ${locator_checkboxauto2}    
    # Select list Nationality
    Select list SQL
    Select list Plan
    Click Element                    ${locator_loginbutton}

Verify Error message Nationnality
    ${txt6}=    Get Text           ${locator_err_nationality}
    Should Be Equal As Strings     ${txt6}                      This field is required

Input Data for fail - Role
    Input Text                  ${locator_firstname}    นลินี
    Input Text                  ${locator_lastname}     สุจริต
    Input Text                  ${locator_email}        Beam1234@gmail.com
    Input Password              ${locator_password}     1234567890
    Input Text                  ${locator_mobilephone}  0824814195    
    Select Radio Button         ${locator_gender}       female
    Select Checkbox             ${locator_checkboxSQL}
    Select Checkbox             ${locator_checkboxmanual} 
    Select Checkbox             ${locator_checkboxauto}   
    Select Checkbox             ${locator_checkboxauto2}    
    Select list Nationality
    # Select list SQL
    Select list Plan
    Click Element                    ${locator_loginbutton}

Verify Error message Role
    ${txt6}=    Get Text           ${locator_err_role}
    Should Be Equal As Strings     ${txt6}                      This field is required

Input Data for fail - Plan
    Input Text                  ${locator_firstname}    นลินี
    Input Text                  ${locator_lastname}     สุจริต
    Input Text                  ${locator_email}        Beam1234@gmail.com
    Input Password              ${locator_password}     1234567890
    Input Text                  ${locator_mobilephone}  0824814195    
    Select Radio Button         ${locator_gender}       female
    Select Checkbox             ${locator_checkboxSQL}
    Select Checkbox             ${locator_checkboxmanual} 
    Select Checkbox             ${locator_checkboxauto}   
    Select Checkbox             ${locator_checkboxauto2}    
    Select list Nationality
    Select list SQL
    # Select list Plan
    Click Element                    ${locator_loginbutton}

Verify Error message Plan
    ${txt6}=    Get Text           ${locator_err_plan}
    Should Be Equal As Strings     ${txt6}                      This field is required

Input Data for fail - Wrong Email
    Input Text                  ${locator_firstname}    นลินี
    Input Text                  ${locator_lastname}     สุจริต
    Input Text                  ${locator_email}        Beam1234gmail.com
    Input Password              ${locator_password}     1234567890
    Input Text                  ${locator_mobilephone}  0824814195      
    Select Radio Button         ${locator_gender}       female
    Select Checkbox             ${locator_checkboxSQL}
    Select Checkbox             ${locator_checkboxmanual} 
    Select Checkbox             ${locator_checkboxauto}   
    Select Checkbox             ${locator_checkboxauto2}    
    Select list Nationality
    Select list SQL
    Select list Plan
    Click Element                    ${locator_loginbutton}

Verify Error message Email2
    ${txt2}=    Get Text           ${locator_err_wrongmail}
    Should Be Equal As Strings     ${txt2}                      Invalid email address


*** Test Cases ***

TC001-Input Data Pass
    Open Webbrowser
    Input Data for registor pass
    Close Browser

TC002-Reset Data
    Open Webbrowser
    Input Data
    Click Element    ${locator_reset}
    Close Browser   

TC003-Verify Firstname-fail
    Open Webbrowser
    Input Data for fail - Firstname
    Verify Error message
    Close Browser

TC004-Verify Lastname-fail
    Open Webbrowser
    Input Data for fail - Lastname
    Verify Error message Lastname
    Close Browser

TC005-Verify Email-fail
    Open Webbrowser
    Input Data for fail - Email
    Verify Error message Email
    Close Browser

TC006-Verify Password-fail
    Open Webbrowser
    Input Data for fail - Password
    Verify Error message Password
    Close Browser

TC007-Verify Phone-fail
    Open Webbrowser
    Input Data for fail - Phone
    Verify Error message Phone
    Close Browser

TC008-Verify Gender-fail
    Open Webbrowser
    Input Data for fail - Gender
    Verify Error message Gender
    Close Browser

TC009-Verify Checkbox-fail
    Open Webbrowser
    Input Data for fail - Checkbox
    Verify Error message Checkbox
    Close Browser

TC010-Verify Nationality-fail
    Open Webbrowser
    Input Data for fail - Nationnality
    Verify Error message Nationnality
    Close Browser

TC011-Verify Role-fail
    Open Webbrowser
    Input Data for fail - Role
    Verify Error message Role
    Close Browser

TC012-Verify Plan-fail
    Open Webbrowser
    Input Data for fail - Plan
    Verify Error message Plan
    Close Browser

TC013-Verify Wrong Email-fail
    Open Webbrowser
    Input Data for fail - Wrong Email
    Verify Error message Email2
    Close Browser


